#!/usr/bin/env python2
from __future__ import print_function, division

import argparse, os, re, sys
import time
import Bio.Entrez

def error(*things):
	print('[ERROR]:', *things, file=sys.stderr)
	exit(1)

def menu(optiondict, header='Choose an option:', prompt='?= '):

	chosen = 0
	options = sorted(optiondict)
	if len(options) == 1: return options[0]
	while not chosen:
		print(header)
		for i, k in enumerate(options):
			print('{}: {}'.format(i+1, optiondict[options[i]]))
		print(prompt, end='')
		try: s = int(raw_input().strip())
		except ValueError: continue
		if 1 <= int(s) <= len(options):
			chosen = 1
			return options[s-1]

def iter2commas(iterable):
	s = ''
	for x in iterable: s += '{},'.format(x)
	return s[:-1]

def search_taxonomies(query):
	esea = Bio.Entrez.esearch('taxonomy', query)
	record = Bio.Entrez.read(esea)
	esea.close()
	if int(record['Count']): return record['IdList']
	else: return []

def resolve_taxonomy(entry):
	esum = Bio.Entrez.esummary(db='taxonomy', id=entry)
	record = Bio.Entrez.read(esum)
	esum.close()

	menudict = {}
	for k in record: 
		menudict[k['Id']] = '{} {}'.format(k['Rank'], k['ScientificName'])
	return menu(menudict)

def get_all_species(entry, specieslist=None):
	if specieslist is None: specieslist = []

	esum = Bio.Entrez.esummary(db='taxonomy', id=entry)
	record = Bio.Entrez.read(esum)
	esum.close()

	for k in record: 
		if k['Rank'] in ('species', 'strain'): 
			specieslist.append(k['Id'])
			return specieslist
		else:
			elin = Bio.Entrez.elink(db='taxonomy', dbfrom='taxonomy', id=k['Id'])
			outrecord = Bio.Entrez.read(elin)
			elin.close()

			for l in outrecord:
				for m in l['LinkSetDb']:
					for child in m['Link']:
						#get_all_species(child['Id'], specieslist)
						get_all_species(child['Id'], specieslist)

def get_all_genomes(species):
	elin = Bio.Entrez.elink(db='genome', dbfrom='taxonomy', id=species)
	record = Bio.Entrez.read(elin)
	elin.close()

	genomelist = []
	for k in record:
		for l in k['LinkSetDb']:
			for m in l['Link']:
				genomelist.append(m['Id'])
	return genomelist

def fetch_genome(genome):
	efet = Bio.Entrez.efetch(db='genome', id=genome)
	record = Bio.Entrez.read(efet)
	efet.close()

	#for r in record: print('genome', genome, r)

	esum = Bio.Entrez.esummary(db='genome', id=genome)
	record = Bio.Entrez.read(esum)
	esum.close()
	for r in record: print('genome', genome, r)
def main(query, maxhits=1000):
	#1. list all related taxonomies
	taxonomies = search_taxonomies(query)
	#for entry in taxonomies: resolve_taxonomy(entry)
	root = resolve_taxonomy(iter2commas(taxonomies))

	all_species = []
	get_all_species(root, all_species)
	genome_ids = get_all_genomes(all_species)

	for g in genome_ids:
		fetch_genome(g)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	if 'ENTREZ_EMAIL' in os.environ: parser.add_argument('-e', '--email', default=os.environ['ENTREZ_EMAIL'], help='Somewhere NCBI can contact if it becomes necessary. Defaults to $ENTREZ_EMAIL if set. (default: {})'.format(os.environ['ENTREZ_EMAIL']))
	else: parser.add_argument('-e', '--email', help='Somewhere NCBI can contact if it becomes necessary. Defaults to $ENTREZ_EMAIL if set. (default: None)')

	parser.add_argument('-n', type=int, default=1000, help='Maximum number of results to handle')

	parser.add_argument('query', help='What to search for')

	args = parser.parse_args()

	if not args.email: error('Please specify an email or set $ENTREZ_EMAIL to your email')

	Bio.Entrez.email = args.email
	Bio.Entrez.tool = 'biopython:lariat'

	main(args.query, maxhits=args.n)
